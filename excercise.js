//	c130710a Usuario de BitBucket

//	Wheel "class"
function Wheel(rad){
	this.radius = rad || 0;
	if (!Wheel.prototype.activate) {
		Wheel.prototype.activate = function() {
			return 2 * Math.PI * this.radius;
		}
	}
}

//	PropellingNozzle "class"
function PropellingNozzle(power){
	this.power = power;
	this.isAfterburnOn = false;

	if (!PropellingNozzle.prototype.activate) {
		//	PropellingNozzle "methods"
		PropellingNozzle.prototype.activate = function() {
			return this.power + this.power * this.isAfterburnOn;
		}
	}
}

//	Propeller "class"
function Propeller(finsNumber, clockwiseSpin) {
	this.fins = finsNumber;
	this.isSpinDirectionClockwise = clockwiseSpin || true; //	true iif the spin direction is clockwise

	if (!Propeller.prototype.activate) {
		//	Propeller "methods"
		Propeller.prototype.activate = function() {
			if(this.isSpinDirectionClockwise) {
				return this.fins;
			}
			return this.fins * -1;
		};
		Propeller.prototype.switchSpin = function () {
			this.isSpinDirectionClockwise = !this.isSpinDirectionClockwise;
		};
		Propeller.prototype.switchSpinTo = function(newSpinOrientation) {
			if(newSpinOrientation === false || newSpinOrientation === "counter-clockwise") {
				this.isSpinDirectionClockwise = false;
			} else {
				this.isSpinDirectionClockwise = true;
			}
		};
	}
}
////////////////////////////

function Vehicle() {
	this.velocity = 0;
	this.propulsion = {};
	if (!Vehicle.prototype.accelerate) {
		Vehicle.prototype.accelerate = function() {
			this.velocity += this.activatePropulsors();
			console.log("The new velocity is: " + this.velocity);
		}
	}
}

function LandVehicle(wheelRadius) {
	wheelRadius = wheelRadius || 0;
	vehicle = new Vehicle();
	vehicle.propulsion.wheel1 = new Wheel(wheelRadius);
	vehicle.propulsion.wheel2 = new Wheel(wheelRadius);
	vehicle.propulsion.wheel3 = new Wheel(wheelRadius);
	vehicle.propulsion.wheel4 = new Wheel(wheelRadius);
	vehicle.__proto__ = LandVehicle.prototype;
	return vehicle;
}
LandVehicle.prototype = new Vehicle();
LandVehicle.prototype.activatePropulsors = function() {
	var acceleration = 0;
	for(propulsor in this.propulsion) {
		acceleration += this.propulsion[propulsor].activate();
	}
	return acceleration;
}


function AirVehicle() {
	var vehicle = new Vehicle();
	vehicle.propulsion.propellingNozzle = new PropellingNozzle(10);
	vehicle.__proto__ = AirVehicle.prototype;
	return vehicle;
}
AirVehicle.prototype = new Vehicle();
AirVehicle.prototype.activatePropulsors = function() {
	return this.propulsion.propellingNozzle.activate();
}
AirVehicle.prototype.setAfterburn = function(boolValue) {
	this.propulsion.propellingNozzle.isAfterburnOn = boolValue;
}

function WaterVehicle(propellersQuantity){
	var vehicle = new Vehicle();
	for(var i = 1; i<=propellersQuantity; i++) {
		vehicle.propulsion["propeller"+i] = new Propeller(5);
	}
	vehicle.__proto__ = WaterVehicle.prototype;
	return vehicle;
}
WaterVehicle.prototype = new Vehicle();

WaterVehicle.prototype.activatePropulsors = function() {
	var acceleration = 0;
	for(propulsor in this.propulsion) {
		acceleration += this.propulsion[propulsor].activate();
	}
	return acceleration;
}
WaterVehicle.prototype.switchSpins = function() {
	for(propulsor in this.propulsion) {
		this.propulsion[propulsor].switchSpin();
	}
}
WaterVehicle.prototype.switchSpinsTo = function(newSpinOrientation) {
	for(propulsor in this.propulsion) {
		this.propulsion[propulsor].switchSpinTo(newSpinOrientation);
	}
}
////////


function AmphibiousVehicle(wheelRadius, propellersQuantity) {
	vehicle = new Vehicle();
	vehicle.isLandModeOn = true;

	//	i set the propulsion by default to "Land"
	vehicle.propulsion.wheel1 = new Wheel(wheelRadius);
	vehicle.propulsion.wheel2 = new Wheel(wheelRadius);
	vehicle.propulsion.wheel3 = new Wheel(wheelRadius);
	vehicle.propulsion.wheel4 = new Wheel(wheelRadius);

	vehicle.alternatePropulsion = {};
	for(var i = 1; i<=propellersQuantity; i++) {
		vehicle.alternatePropulsion["propeller"+i] = new Propeller(5);
	}
	vehicle.__proto__ = AmphibiousVehicle.prototype;
	return vehicle;
}
AmphibiousVehicle.prototype = Vehicle.prototype;

AmphibiousVehicle.prototype.waterMode = function() {
	//	i change things only if the land mode is active
	if (this.isLandModeOn) {
		var temp = this.propulsion;
		this.propulsion = this.alternatePropulsion;
		this.alternatePropulsion = temp;
		this.isLandModeOn = false;
	}
}
AmphibiousVehicle.prototype.landMode = function() {
	//	i change things only if the land mode is inactive
	if (!this.isLandModeOn) {
		var temp = this.propulsion;
		this.propulsion = this.alternatePropulsion;
		this.alternatePropulsion = temp;
		this.isLandModeOn = true;
	}
}
AmphibiousVehicle.prototype.activatePropulsors = function() {
	if(this.isLandModeOn) {
		return LandVehicle.prototype.activatePropulsors.apply(this);
	} else {
		return WaterVehicle.prototype.activatePropulsors.apply(this);
	}
}
AmphibiousVehicle.prototype.switchSpins = function() {
	if (!this.isLandModeOn) {
		for(propulsor in this.propulsion) {
			this.propulsion[propulsor].switchSpin();
		}
	}
}
AmphibiousVehicle.prototype.switchSpinsTo = function(newSpinOrientation) {
	if (!this.isLandModeOn) {
		for(propulsor in this.propulsion) {
			this.propulsion[propulsor].switchSpinTo(newSpinOrientation);
		}
	}
}


car = new LandVehicle(4);
airplane = new AirVehicle();
boat = new WaterVehicle(2);
reptile = new AmphibiousVehicle(5, 2);